# HDR (Humans Detected by Robots)

This repository deals with human detection and tracking in robotic systems where human-robot interaction takes place. Such systems should be safe for operators who are using it. This solution controls the robot's motion based on the position and movement of humans.


### Launch using one kinect sensor
roslaunch bbox_depth bbox_depth.launch

### Launch using two kinect sensors
roslaunch bbox_depth bbox_depth_2_cams.launch
