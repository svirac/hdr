# generated from catkin/cmake/template/pkg.context.pc.in
CATKIN_PACKAGE_PREFIX = ""
PROJECT_PKG_CONFIG_INCLUDE_DIRS = "${prefix}/include".split(';') if "${prefix}/include" != "" else []
PROJECT_CATKIN_DEPENDS = "roscpp;roslib;std_msgs;darknet_ros;darknet_ros_msgs;message_generation;message_runtime".replace(';', ' ')
PKG_CONFIG_LIBRARIES_WITH_PREFIX = "-lBoundingBoxDepthClass".split(';') if "-lBoundingBoxDepthClass" != "" else []
PROJECT_NAME = "bbox_depth"
PROJECT_SPACE_DIR = "/home/damian/catkin_ws/install"
PROJECT_VERSION = "0.0.0"
