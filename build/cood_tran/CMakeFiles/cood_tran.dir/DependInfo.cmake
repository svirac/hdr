# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/damian/catkin_ws/src/cood_tran/src/cood_tran.cpp" "/home/damian/catkin_ws/build/cood_tran/CMakeFiles/cood_tran.dir/src/cood_tran.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"cood_tran\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/damian/catkin_ws/src/cood_tran/include"
  "/home/damian/catkin_ws/devel/include"
  "/home/damian/catkin_ws/src/darknet_ros/darknet_ros/include"
  "/opt/ros/kinetic/include"
  "/opt/ros/kinetic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/ros/kinetic/include/opencv-3.3.1-dev"
  "/opt/ros/kinetic/include/opencv-3.3.1-dev/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/damian/catkin_ws/build/darknet_ros/darknet_ros/CMakeFiles/darknet_ros_lib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
