# CMake generated Testfile for 
# Source directory: /home/damian/catkin_ws/src
# Build directory: /home/damian/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(realsense-ros/realsense2_description)
subdirs(abb2400)
subdirs(openni_camera/openni_description)
subdirs(openni_camera/openni_launch)
subdirs(darknet_ros/darknet_ros_msgs)
subdirs(ros_astra_camera)
subdirs(darknet_ros/darknet_ros)
subdirs(bbox_depth)
subdirs(abb_control)
subdirs(openni_camera/openni_camera)
subdirs(sort_track)
subdirs(realsense-ros/realsense2_camera)
