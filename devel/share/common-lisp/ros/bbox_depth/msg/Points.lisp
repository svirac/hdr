; Auto-generated. Do not edit!


(cl:in-package bbox_depth-msg)


;//! \htmlinclude Points.msg.html

(cl:defclass <Points> (roslisp-msg-protocol:ros-message)
  ((coords
    :reader coords
    :initarg :coords
    :type (cl:vector bbox_depth-msg:Coords)
   :initform (cl:make-array 0 :element-type 'bbox_depth-msg:Coords :initial-element (cl:make-instance 'bbox_depth-msg:Coords))))
)

(cl:defclass Points (<Points>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Points>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Points)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name bbox_depth-msg:<Points> is deprecated: use bbox_depth-msg:Points instead.")))

(cl:ensure-generic-function 'coords-val :lambda-list '(m))
(cl:defmethod coords-val ((m <Points>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader bbox_depth-msg:coords-val is deprecated.  Use bbox_depth-msg:coords instead.")
  (coords m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Points>) ostream)
  "Serializes a message object of type '<Points>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'coords))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'coords))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Points>) istream)
  "Deserializes a message object of type '<Points>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'coords) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'coords)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'bbox_depth-msg:Coords))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Points>)))
  "Returns string type for a message object of type '<Points>"
  "bbox_depth/Points")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Points)))
  "Returns string type for a message object of type 'Points"
  "bbox_depth/Points")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Points>)))
  "Returns md5sum for a message object of type '<Points>"
  "9a00924c5b980fe809fce34d3b5d9735")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Points)))
  "Returns md5sum for a message object of type 'Points"
  "9a00924c5b980fe809fce34d3b5d9735")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Points>)))
  "Returns full string definition for message of type '<Points>"
  (cl:format cl:nil "Coords[] coords~%~%================================================================================~%MSG: bbox_depth/Coords~%int32 x~%int32 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Points)))
  "Returns full string definition for message of type 'Points"
  (cl:format cl:nil "Coords[] coords~%~%================================================================================~%MSG: bbox_depth/Coords~%int32 x~%int32 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Points>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'coords) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Points>))
  "Converts a ROS message object to a list"
  (cl:list 'Points
    (cl:cons ':coords (coords msg))
))
