; Auto-generated. Do not edit!


(cl:in-package bbox_depth-msg)


;//! \htmlinclude Points_array.msg.html

(cl:defclass <Points_array> (roslisp-msg-protocol:ros-message)
  ((points_coords
    :reader points_coords
    :initarg :points_coords
    :type (cl:vector bbox_depth-msg:Points_coords)
   :initform (cl:make-array 0 :element-type 'bbox_depth-msg:Points_coords :initial-element (cl:make-instance 'bbox_depth-msg:Points_coords))))
)

(cl:defclass Points_array (<Points_array>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Points_array>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Points_array)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name bbox_depth-msg:<Points_array> is deprecated: use bbox_depth-msg:Points_array instead.")))

(cl:ensure-generic-function 'points_coords-val :lambda-list '(m))
(cl:defmethod points_coords-val ((m <Points_array>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader bbox_depth-msg:points_coords-val is deprecated.  Use bbox_depth-msg:points_coords instead.")
  (points_coords m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Points_array>) ostream)
  "Serializes a message object of type '<Points_array>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'points_coords))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'points_coords))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Points_array>) istream)
  "Deserializes a message object of type '<Points_array>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'points_coords) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'points_coords)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'bbox_depth-msg:Points_coords))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Points_array>)))
  "Returns string type for a message object of type '<Points_array>"
  "bbox_depth/Points_array")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Points_array)))
  "Returns string type for a message object of type 'Points_array"
  "bbox_depth/Points_array")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Points_array>)))
  "Returns md5sum for a message object of type '<Points_array>"
  "d740a7bcd2867f8164258ffb96a46238")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Points_array)))
  "Returns md5sum for a message object of type 'Points_array"
  "d740a7bcd2867f8164258ffb96a46238")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Points_array>)))
  "Returns full string definition for message of type '<Points_array>"
  (cl:format cl:nil "Points_coords[] points_coords~%~%================================================================================~%MSG: bbox_depth/Points_coords~%int32 x~%int32 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Points_array)))
  "Returns full string definition for message of type 'Points_array"
  (cl:format cl:nil "Points_coords[] points_coords~%~%================================================================================~%MSG: bbox_depth/Points_coords~%int32 x~%int32 y~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Points_array>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'points_coords) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Points_array>))
  "Converts a ROS message object to a list"
  (cl:list 'Points_array
    (cl:cons ':points_coords (points_coords msg))
))
