
(cl:in-package :asdf)

(defsystem "bbox_depth-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "Regulator" :depends-on ("_package_Regulator"))
    (:file "_package_Regulator" :depends-on ("_package"))
  ))