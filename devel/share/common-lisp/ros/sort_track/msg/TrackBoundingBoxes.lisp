; Auto-generated. Do not edit!


(cl:in-package sort_track-msg)


;//! \htmlinclude TrackBoundingBoxes.msg.html

(cl:defclass <TrackBoundingBoxes> (roslisp-msg-protocol:ros-message)
  ((bounding_boxes
    :reader bounding_boxes
    :initarg :bounding_boxes
    :type (cl:vector sort_track-msg:TrackBoundingBox)
   :initform (cl:make-array 0 :element-type 'sort_track-msg:TrackBoundingBox :initial-element (cl:make-instance 'sort_track-msg:TrackBoundingBox))))
)

(cl:defclass TrackBoundingBoxes (<TrackBoundingBoxes>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TrackBoundingBoxes>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TrackBoundingBoxes)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name sort_track-msg:<TrackBoundingBoxes> is deprecated: use sort_track-msg:TrackBoundingBoxes instead.")))

(cl:ensure-generic-function 'bounding_boxes-val :lambda-list '(m))
(cl:defmethod bounding_boxes-val ((m <TrackBoundingBoxes>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader sort_track-msg:bounding_boxes-val is deprecated.  Use sort_track-msg:bounding_boxes instead.")
  (bounding_boxes m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TrackBoundingBoxes>) ostream)
  "Serializes a message object of type '<TrackBoundingBoxes>"
  (cl:let ((__ros_arr_len (cl:length (cl:slot-value msg 'bounding_boxes))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_arr_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_arr_len) ostream))
  (cl:map cl:nil #'(cl:lambda (ele) (roslisp-msg-protocol:serialize ele ostream))
   (cl:slot-value msg 'bounding_boxes))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TrackBoundingBoxes>) istream)
  "Deserializes a message object of type '<TrackBoundingBoxes>"
  (cl:let ((__ros_arr_len 0))
    (cl:setf (cl:ldb (cl:byte 8 0) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 8) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 16) __ros_arr_len) (cl:read-byte istream))
    (cl:setf (cl:ldb (cl:byte 8 24) __ros_arr_len) (cl:read-byte istream))
  (cl:setf (cl:slot-value msg 'bounding_boxes) (cl:make-array __ros_arr_len))
  (cl:let ((vals (cl:slot-value msg 'bounding_boxes)))
    (cl:dotimes (i __ros_arr_len)
    (cl:setf (cl:aref vals i) (cl:make-instance 'sort_track-msg:TrackBoundingBox))
  (roslisp-msg-protocol:deserialize (cl:aref vals i) istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TrackBoundingBoxes>)))
  "Returns string type for a message object of type '<TrackBoundingBoxes>"
  "sort_track/TrackBoundingBoxes")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TrackBoundingBoxes)))
  "Returns string type for a message object of type 'TrackBoundingBoxes"
  "sort_track/TrackBoundingBoxes")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TrackBoundingBoxes>)))
  "Returns md5sum for a message object of type '<TrackBoundingBoxes>"
  "334d44e46c7998c23d142060e99a5c30")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TrackBoundingBoxes)))
  "Returns md5sum for a message object of type 'TrackBoundingBoxes"
  "334d44e46c7998c23d142060e99a5c30")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TrackBoundingBoxes>)))
  "Returns full string definition for message of type '<TrackBoundingBoxes>"
  (cl:format cl:nil "TrackBoundingBox[] bounding_boxes~%~%================================================================================~%MSG: sort_track/TrackBoundingBox~%int64 xmin~%int64 ymin~%int64 xmax~%int64 ymax~%int32 id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TrackBoundingBoxes)))
  "Returns full string definition for message of type 'TrackBoundingBoxes"
  (cl:format cl:nil "TrackBoundingBox[] bounding_boxes~%~%================================================================================~%MSG: sort_track/TrackBoundingBox~%int64 xmin~%int64 ymin~%int64 xmax~%int64 ymax~%int32 id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TrackBoundingBoxes>))
  (cl:+ 0
     4 (cl:reduce #'cl:+ (cl:slot-value msg 'bounding_boxes) :key #'(cl:lambda (ele) (cl:declare (cl:ignorable ele)) (cl:+ (roslisp-msg-protocol:serialization-length ele))))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TrackBoundingBoxes>))
  "Converts a ROS message object to a list"
  (cl:list 'TrackBoundingBoxes
    (cl:cons ':bounding_boxes (bounding_boxes msg))
))
