; Auto-generated. Do not edit!


(cl:in-package sort_track-msg)


;//! \htmlinclude TrackCount.msg.html

(cl:defclass <TrackCount> (roslisp-msg-protocol:ros-message)
  ((count
    :reader count
    :initarg :count
    :type cl:integer
    :initform 0))
)

(cl:defclass TrackCount (<TrackCount>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <TrackCount>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'TrackCount)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name sort_track-msg:<TrackCount> is deprecated: use sort_track-msg:TrackCount instead.")))

(cl:ensure-generic-function 'count-val :lambda-list '(m))
(cl:defmethod count-val ((m <TrackCount>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader sort_track-msg:count-val is deprecated.  Use sort_track-msg:count instead.")
  (count m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <TrackCount>) ostream)
  "Serializes a message object of type '<TrackCount>"
  (cl:let* ((signed (cl:slot-value msg 'count)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <TrackCount>) istream)
  "Deserializes a message object of type '<TrackCount>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'count) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<TrackCount>)))
  "Returns string type for a message object of type '<TrackCount>"
  "sort_track/TrackCount")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'TrackCount)))
  "Returns string type for a message object of type 'TrackCount"
  "sort_track/TrackCount")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<TrackCount>)))
  "Returns md5sum for a message object of type '<TrackCount>"
  "602d642babe509c7c59f497c23e716a9")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'TrackCount)))
  "Returns md5sum for a message object of type 'TrackCount"
  "602d642babe509c7c59f497c23e716a9")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<TrackCount>)))
  "Returns full string definition for message of type '<TrackCount>"
  (cl:format cl:nil "int32 count~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'TrackCount)))
  "Returns full string definition for message of type 'TrackCount"
  (cl:format cl:nil "int32 count~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <TrackCount>))
  (cl:+ 0
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <TrackCount>))
  "Converts a ROS message object to a list"
  (cl:list 'TrackCount
    (cl:cons ':count (count msg))
))
