;; Auto-generated. Do not edit!


(when (boundp 'sort_track::TrackCount)
  (if (not (find-package "SORT_TRACK"))
    (make-package "SORT_TRACK"))
  (shadow 'TrackCount (find-package "SORT_TRACK")))
(unless (find-package "SORT_TRACK::TRACKCOUNT")
  (make-package "SORT_TRACK::TRACKCOUNT"))

(in-package "ROS")
;;//! \htmlinclude TrackCount.msg.html


(defclass sort_track::TrackCount
  :super ros::object
  :slots (_count ))

(defmethod sort_track::TrackCount
  (:init
   (&key
    ((:count __count) 0)
    )
   (send-super :init)
   (setq _count (round __count))
   self)
  (:count
   (&optional __count)
   (if __count (setq _count __count)) _count)
  (:serialization-length
   ()
   (+
    ;; int32 _count
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _count
       (write-long _count s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _count
     (setq _count (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get sort_track::TrackCount :md5sum-) "602d642babe509c7c59f497c23e716a9")
(setf (get sort_track::TrackCount :datatype-) "sort_track/TrackCount")
(setf (get sort_track::TrackCount :definition-)
      "int32 count

")



(provide :sort_track/TrackCount "602d642babe509c7c59f497c23e716a9")


