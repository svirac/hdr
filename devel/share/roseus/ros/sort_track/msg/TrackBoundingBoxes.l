;; Auto-generated. Do not edit!


(when (boundp 'sort_track::TrackBoundingBoxes)
  (if (not (find-package "SORT_TRACK"))
    (make-package "SORT_TRACK"))
  (shadow 'TrackBoundingBoxes (find-package "SORT_TRACK")))
(unless (find-package "SORT_TRACK::TRACKBOUNDINGBOXES")
  (make-package "SORT_TRACK::TRACKBOUNDINGBOXES"))

(in-package "ROS")
;;//! \htmlinclude TrackBoundingBoxes.msg.html


(defclass sort_track::TrackBoundingBoxes
  :super ros::object
  :slots (_bounding_boxes ))

(defmethod sort_track::TrackBoundingBoxes
  (:init
   (&key
    ((:bounding_boxes __bounding_boxes) (let (r) (dotimes (i 0) (push (instance sort_track::TrackBoundingBox :init) r)) r))
    )
   (send-super :init)
   (setq _bounding_boxes __bounding_boxes)
   self)
  (:bounding_boxes
   (&rest __bounding_boxes)
   (if (keywordp (car __bounding_boxes))
       (send* _bounding_boxes __bounding_boxes)
     (progn
       (if __bounding_boxes (setq _bounding_boxes (car __bounding_boxes)))
       _bounding_boxes)))
  (:serialization-length
   ()
   (+
    ;; sort_track/TrackBoundingBox[] _bounding_boxes
    (apply #'+ (send-all _bounding_boxes :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; sort_track/TrackBoundingBox[] _bounding_boxes
     (write-long (length _bounding_boxes) s)
     (dolist (elem _bounding_boxes)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; sort_track/TrackBoundingBox[] _bounding_boxes
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _bounding_boxes (let (r) (dotimes (i n) (push (instance sort_track::TrackBoundingBox :init) r)) r))
     (dolist (elem- _bounding_boxes)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get sort_track::TrackBoundingBoxes :md5sum-) "334d44e46c7998c23d142060e99a5c30")
(setf (get sort_track::TrackBoundingBoxes :datatype-) "sort_track/TrackBoundingBoxes")
(setf (get sort_track::TrackBoundingBoxes :definition-)
      "TrackBoundingBox[] bounding_boxes

================================================================================
MSG: sort_track/TrackBoundingBox
int64 xmin
int64 ymin
int64 xmax
int64 ymax
int32 id

")



(provide :sort_track/TrackBoundingBoxes "334d44e46c7998c23d142060e99a5c30")


