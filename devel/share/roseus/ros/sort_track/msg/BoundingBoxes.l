;; Auto-generated. Do not edit!


(when (boundp 'sort_track::BoundingBoxes)
  (if (not (find-package "SORT_TRACK"))
    (make-package "SORT_TRACK"))
  (shadow 'BoundingBoxes (find-package "SORT_TRACK")))
(unless (find-package "SORT_TRACK::BOUNDINGBOXES")
  (make-package "SORT_TRACK::BOUNDINGBOXES"))

(in-package "ROS")
;;//! \htmlinclude BoundingBoxes.msg.html


(defclass sort_track::BoundingBoxes
  :super ros::object
  :slots (_bounding_boxes ))

(defmethod sort_track::BoundingBoxes
  (:init
   (&key
    ((:bounding_boxes __bounding_boxes) (let (r) (dotimes (i 0) (push (instance sort_track::BoundingBox :init) r)) r))
    )
   (send-super :init)
   (setq _bounding_boxes __bounding_boxes)
   self)
  (:bounding_boxes
   (&rest __bounding_boxes)
   (if (keywordp (car __bounding_boxes))
       (send* _bounding_boxes __bounding_boxes)
     (progn
       (if __bounding_boxes (setq _bounding_boxes (car __bounding_boxes)))
       _bounding_boxes)))
  (:serialization-length
   ()
   (+
    ;; sort_track/BoundingBox[] _bounding_boxes
    (apply #'+ (send-all _bounding_boxes :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; sort_track/BoundingBox[] _bounding_boxes
     (write-long (length _bounding_boxes) s)
     (dolist (elem _bounding_boxes)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; sort_track/BoundingBox[] _bounding_boxes
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _bounding_boxes (let (r) (dotimes (i n) (push (instance sort_track::BoundingBox :init) r)) r))
     (dolist (elem- _bounding_boxes)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get sort_track::BoundingBoxes :md5sum-) "777f34cbfe353ad1bb2ecfbf6d86b50c")
(setf (get sort_track::BoundingBoxes :datatype-) "sort_track/BoundingBoxes")
(setf (get sort_track::BoundingBoxes :definition-)
      "BoundingBox[] bounding_boxes

================================================================================
MSG: sort_track/BoundingBox
int32 xmin
int32 ymin
int32 xmax
int32 ymax
int32 id

")



(provide :sort_track/BoundingBoxes "777f34cbfe353ad1bb2ecfbf6d86b50c")


