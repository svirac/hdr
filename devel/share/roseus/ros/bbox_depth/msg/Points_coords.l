;; Auto-generated. Do not edit!


(when (boundp 'bbox_depth::Points_coords)
  (if (not (find-package "BBOX_DEPTH"))
    (make-package "BBOX_DEPTH"))
  (shadow 'Points_coords (find-package "BBOX_DEPTH")))
(unless (find-package "BBOX_DEPTH::POINTS_COORDS")
  (make-package "BBOX_DEPTH::POINTS_COORDS"))

(in-package "ROS")
;;//! \htmlinclude Points_coords.msg.html


(defclass bbox_depth::Points_coords
  :super ros::object
  :slots (_x _y ))

(defmethod bbox_depth::Points_coords
  (:init
   (&key
    ((:x __x) 0)
    ((:y __y) 0)
    )
   (send-super :init)
   (setq _x (round __x))
   (setq _y (round __y))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:serialization-length
   ()
   (+
    ;; int32 _x
    4
    ;; int32 _y
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; int32 _x
       (write-long _x s)
     ;; int32 _y
       (write-long _y s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; int32 _x
     (setq _x (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; int32 _y
     (setq _y (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get bbox_depth::Points_coords :md5sum-) "bd7b43fd41d4c47bf5c703cc7d016709")
(setf (get bbox_depth::Points_coords :datatype-) "bbox_depth/Points_coords")
(setf (get bbox_depth::Points_coords :definition-)
      "int32 x
int32 y

")



(provide :bbox_depth/Points_coords "bd7b43fd41d4c47bf5c703cc7d016709")


