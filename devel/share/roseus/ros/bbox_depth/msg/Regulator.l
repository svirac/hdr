;; Auto-generated. Do not edit!


(when (boundp 'bbox_depth::Regulator)
  (if (not (find-package "BBOX_DEPTH"))
    (make-package "BBOX_DEPTH"))
  (shadow 'Regulator (find-package "BBOX_DEPTH")))
(unless (find-package "BBOX_DEPTH::REGULATOR")
  (make-package "BBOX_DEPTH::REGULATOR"))

(in-package "ROS")
;;//! \htmlinclude Regulator.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(defclass bbox_depth::Regulator
  :super ros::object
  :slots (_header _value ))

(defmethod bbox_depth::Regulator
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:value __value) 0)
    )
   (send-super :init)
   (setq _header __header)
   (setq _value (round __value))
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:value
   (&optional __value)
   (if __value (setq _value __value)) _value)
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; int32 _value
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; int32 _value
       (write-long _value s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; int32 _value
     (setq _value (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;;
   self)
  )

(setf (get bbox_depth::Regulator :md5sum-) "f231299e824089b22a987717bd7ace62")
(setf (get bbox_depth::Regulator :datatype-) "bbox_depth/Regulator")
(setf (get bbox_depth::Regulator :definition-)
      "Header header
int32 value

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

")



(provide :bbox_depth/Regulator "f231299e824089b22a987717bd7ace62")


