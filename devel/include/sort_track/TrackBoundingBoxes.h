// Generated by gencpp from file sort_track/TrackBoundingBoxes.msg
// DO NOT EDIT!


#ifndef SORT_TRACK_MESSAGE_TRACKBOUNDINGBOXES_H
#define SORT_TRACK_MESSAGE_TRACKBOUNDINGBOXES_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <sort_track/TrackBoundingBox.h>

namespace sort_track
{
template <class ContainerAllocator>
struct TrackBoundingBoxes_
{
  typedef TrackBoundingBoxes_<ContainerAllocator> Type;

  TrackBoundingBoxes_()
    : bounding_boxes()  {
    }
  TrackBoundingBoxes_(const ContainerAllocator& _alloc)
    : bounding_boxes(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector< ::sort_track::TrackBoundingBox_<ContainerAllocator> , typename ContainerAllocator::template rebind< ::sort_track::TrackBoundingBox_<ContainerAllocator> >::other >  _bounding_boxes_type;
  _bounding_boxes_type bounding_boxes;





  typedef boost::shared_ptr< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> const> ConstPtr;

}; // struct TrackBoundingBoxes_

typedef ::sort_track::TrackBoundingBoxes_<std::allocator<void> > TrackBoundingBoxes;

typedef boost::shared_ptr< ::sort_track::TrackBoundingBoxes > TrackBoundingBoxesPtr;
typedef boost::shared_ptr< ::sort_track::TrackBoundingBoxes const> TrackBoundingBoxesConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::sort_track::TrackBoundingBoxes_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace sort_track

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'sort_track': ['/home/damian/catkin_ws/src/sort_track/msg', '/home/damian/catkin_ws/src/sort_track/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
{
  static const char* value()
  {
    return "334d44e46c7998c23d142060e99a5c30";
  }

  static const char* value(const ::sort_track::TrackBoundingBoxes_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x334d44e46c7998c2ULL;
  static const uint64_t static_value2 = 0x3d142060e99a5c30ULL;
};

template<class ContainerAllocator>
struct DataType< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
{
  static const char* value()
  {
    return "sort_track/TrackBoundingBoxes";
  }

  static const char* value(const ::sort_track::TrackBoundingBoxes_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
{
  static const char* value()
  {
    return "TrackBoundingBox[] bounding_boxes\n\
\n\
================================================================================\n\
MSG: sort_track/TrackBoundingBox\n\
int64 xmin\n\
int64 ymin\n\
int64 xmax\n\
int64 ymax\n\
int32 id\n\
";
  }

  static const char* value(const ::sort_track::TrackBoundingBoxes_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.bounding_boxes);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct TrackBoundingBoxes_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::sort_track::TrackBoundingBoxes_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::sort_track::TrackBoundingBoxes_<ContainerAllocator>& v)
  {
    s << indent << "bounding_boxes[]" << std::endl;
    for (size_t i = 0; i < v.bounding_boxes.size(); ++i)
    {
      s << indent << "  bounding_boxes[" << i << "]: ";
      s << std::endl;
      s << indent;
      Printer< ::sort_track::TrackBoundingBox_<ContainerAllocator> >::stream(s, indent + "    ", v.bounding_boxes[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // SORT_TRACK_MESSAGE_TRACKBOUNDINGBOXES_H
