/*
	Node instance using Class node pattern.
*/


#include "BoundingBoxDepthClass.hpp"


int main(int argc, char** argv)
{
	std::string node_name = "bbox_depth";
	ros::init(argc, argv, node_name);
	ros::NodeHandle nh("~");
	BoundingBoxDepthClass node(nh);
	ROS_INFO("Initialized single-thread class node.");
	ros::spin();
	

	
	
	return 0;
}

