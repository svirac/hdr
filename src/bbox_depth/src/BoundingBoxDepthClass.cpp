#include "BoundingBoxDepthClass.hpp"


// Constructor //
BoundingBoxDepthClass::BoundingBoxDepthClass(const ros::NodeHandle &node_handle):
	nh(node_handle), width(640), height(480), fx(1/554.545), fy(1/552.145), cx(327.799), cy(236.814),
	envelope_default_radius(2.0)
{
	nh.param("init_depths_file", initial_depths_filename, std::string("init.csv"));
	pkg_path = ros::package::getPath("bbox_depth");
	initial_depths_filename = pkg_path + initial_depths_filename;
	loadInitialDepths(width, height);

	int u = 467;
	int v = 233;

	/*// * Calculating robot point in camera's CS
	float z = initial_depths[u][v];
	float x = z * ((u - cx) * fx);
	float y = z * ((v - cy) * fy);

	robot.x = x;
	robot.y = y;
	robot.z = z;
	*/

	nh.param("robot_position/x", robot.x, float(0.0));
	nh.param("robot_position/y", robot.y, float(0.0));
	nh.param("robot_position/z", robot.z, float(0.0));

	// Robot envelope radius
	robot.r = envelope_default_radius;

  bbox_count = 0;
	frame_counter = 0;
	current_frame = 0;
	last_frame = 0;
	frame_diff = 0;
	frame_threshold = 10;

	this->init();
}


void BoundingBoxDepthClass::init()
{
	// Get topic names
	nh.param("subscribers/rgb_img_topic", rgb_img_topic, std::string("/camera/rgb/image_color"));
	nh.param("subscribers/depth_img_topic", depth_img_topic, std::string("/camera/depth/image"));
	nh.param("subscribers/yolo_count_topic", yolo_count_topic, std::string("/darknet_ros/found_object"));
	nh.param("subscribers/tracker_bbox_topic", tracker_bbox_topic, std::string("/sort_track/bounding_boxes"));
	nh.param("publishers/regulator_topic", regulator_topic, std::string("/bbox_depth/regulator_value"));

	regulator_publisher = nh.advertise<bbox_depth::Regulator>(regulator_topic, 1);

	sub_yolo_count = nh.subscribe(yolo_count_topic,1, &BoundingBoxDepthClass::yoloCountCallback, this);
	sub_tracker_bbox = nh.subscribe(tracker_bbox_topic,1, &BoundingBoxDepthClass::trackerBboxCallback, this);
	image_transport::ImageTransport it(nh);
	sub_depth_image = it.subscribe(depth_img_topic, 1, &BoundingBoxDepthClass::depthImageCallback, this);
	sub_rgb_image = it.subscribe(rgb_img_topic, 1, &BoundingBoxDepthClass::rgbImageCallback, this);

	/* // Uncomment for testing purposes
	// Writing test results to csv file
	nh.param("test_file", test_filename, std::string("test/kinect_test.csv"));
	test_filename = pkg_path + test_filename;
	test_file.open(test_filename);
	test_file<<"TIME,Detections"<<endl;
	*/
}


// Load initial scene in a matrix //
void BoundingBoxDepthClass::loadInitialDepths(const int width, const int height)
{
  ifstream file;
	file.open(initial_depths_filename);
	for(int row = 0; row < height; row++) {
        string line;
        getline(file, line);
        if ( !file.good() )
            break;
        stringstream iss(line);
        for (int col = 0; col < width; col++) {
            string val;
            getline(iss, val, ',');
            if ( !iss.good() )
                break;
            stringstream convertor(val);
            convertor >> initial_depths[col][row];
        }
    }
		file.close();
}

// Get number of people on the scene //
void BoundingBoxDepthClass::yoloCountCallback(const darknet_ros_msgs::ObjectCount::ConstPtr& msg)
{
	current_frame = frame_counter;
	if(msg->count) {
		last_frame = current_frame;
	}
	frame_diff = abs(current_frame - last_frame);
	if(frame_diff > frame_threshold && !msg->count) {
		frame_counter = 0;
	}
}

// Get bounding boxes from Deep SORT //
void BoundingBoxDepthClass::trackerBboxCallback(const sort_track::TrackBoundingBoxes::ConstPtr& msg)
{
	if(frame_diff <= frame_threshold) {
		bbox_count = msg->bounding_boxes.size();
	}
	else {
		bbox_count = 0;
	}

	human_idx_array_old = human_idx_array;
	human_idx_array.clear();

	for(int i = 0; i < bbox_count; i++) {
		int idx = msg->bounding_boxes[i].id;
		human_idx_array.push_back(idx);

		bbox[idx].id = msg->bounding_boxes[i].id;
		bbox[idx].ymin = msg->bounding_boxes[i].ymin;
		bbox[idx].ymax = msg->bounding_boxes[i].ymax;
		bbox[idx].xmin = msg->bounding_boxes[i].xmin;
		bbox[idx].xmax = msg->bounding_boxes[i].xmax;

		if(bbox[idx].xmin <= 0) bbox[idx].xmin = 0;
		if(bbox[idx].xmax >= 640) bbox[idx].xmax = 639;
		if(bbox[idx].ymin <= 0) bbox[idx].ymin = 0;
		if(bbox[idx].ymax >= 480) bbox[idx].ymax = 479;

		bbox[idx].center_x = (int) bbox[idx].xmin + (bbox[idx].xmax - bbox[idx].xmin) / 2;
		bbox[idx].center_y = (int) bbox[idx].ymin + (bbox[idx].ymax - bbox[idx].ymin) / 2;
	}

	for(const int& idx_old : human_idx_array_old) {
		if(bbox_count != 0){
			bool flag_miss = false;
			for(const int& idx_new : human_idx_array) {
				if (idx_old == idx_new) {
					flag_miss = false;
					break;
				}
				else if (idx_old != idx_new && bbox[idx_old].is_inside_roi)
					flag_miss = true;
			}
			if(flag_miss){
				//ROS_INFO("dodan index");
				human_idx_array.push_back(idx_old);
			}
		}
		else if(bbox_count == 0 && bbox[idx_old].is_inside_roi){
			human_idx_array.push_back(idx_old);
		}
	}

	sort(human_idx_array.begin(), human_idx_array.end());
}


// Show detections
void BoundingBoxDepthClass::rgbImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
	rgb_image = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8)->image;

	for(const int& idx : human_idx_array){
		cv::Point pt1(bbox[idx].xmin,bbox[idx].ymin);
		cv::Point pt2(bbox[idx].xmax,bbox[idx].ymax);
		cv::Point pt_txt(bbox[idx].xmin+5,bbox[idx].ymin+30);

		cv::rectangle(rgb_image, pt1, pt2, cv::Scalar(255,150,0), 2);
		cv::putText(rgb_image, to_string(idx), pt_txt , 0, 1, cv::Scalar(255,150,0), 2);
	}
	cv::imshow("RGB image", rgb_image);
	cv::waitKey(30);
}


// Get current depth image, compare to initial depth image, and determine human points //
void BoundingBoxDepthClass::depthImageCallback(const sensor_msgs::ImageConstPtr& msg)
{
	depth_image = cv_bridge::toCvShare(msg, "32FC1")->image;
	frame_counter++;
	// * Image used to display points that represent human
	cv::Mat human_points_img(cv::Size(641, 481), CV_8UC3, cv::Scalar(0, 0, 0));

	vector<float> human_depth_array;
	vector<int> regulator_array;

	for(const int& idx : human_idx_array) {
		human_depth_array.clear();
		float initial_depth = 0.0;
		float current_depth = 0.0;
		float previous_depth = 0.0;
		int j_previous = 0;
		for(int j = bbox[idx].ymin; j < bbox[idx].ymax; j++) {
			int i_previous = 0;
			for(int i = bbox[idx].xmin; i < bbox[idx].xmax; i++) {
				bool human_point = false;
				if( depth_image.at<float>(j,i) != 0 && !isnan(depth_image.at<float>(j,i)) ) {
					current_depth = findAverage(i,j,1);
					initial_depth = findAverage(i,j,0);

					float diff = initial_depth - current_depth;
					if(diff > 0.2) {
						human_point = true;
					}
					if (initial_depth == 0) {
						if(abs(previous_depth-current_depth) < 0.1) {
							human_point = true;
						}
						else if ( abs(previous_depth-current_depth) < 0.3 && (i-i_previous == 0 || j-j_previous == 0)) {
							human_point = true;
				}}}
				if(human_point)
				{
					i_previous = i;
					j_previous = j;
					previous_depth = current_depth;
					human_points_img.at<cv::Vec3b>(cv::Point(i, j)) = {255,0,0};
					human_depth_array.push_back(depth_image.at<float>(j, i));
		}}}

		// * Human is represented by the center of a bounding box
		// * and the middle value of human depth points
		if(human_depth_array.size()>1) {
			sort(human_depth_array.begin(), human_depth_array.end());
			int middle_index = (int) human_depth_array.size() / 2;
			bbox[idx].depth = human_depth_array.at(middle_index-1);
		}
		else if (human_depth_array.size() == 1) {
			bbox[idx].depth = human_depth_array.at(0);
		}
		else {
			bbox[idx].depth = bbox[idx].depth_prev;
		}
		bbox[idx].depth_prev = bbox[idx].depth;

		int u = bbox[idx].center_x;
		int v = bbox[idx].center_y;
		// * Calculating human point in camera's CS
		float z = bbox[idx].depth;
		float x = z * ((u - cx) * fx);
		float y = z * ((v - cy) * fy);

		bbox[idx].time_current = msg->header.stamp;
		bbox[idx].x = x;
		bbox[idx].y = y;
		bbox[idx].z = z;
		// * Setting up previous human position only if the human
		// * showed up for the first time
		if(!bbox[idx].x_prev) {
			bbox[idx].time_prev = bbox[idx].time_current;
			bbox[idx].x_prev = bbox[idx].x;
			bbox[idx].y_prev = bbox[idx].y;
			bbox[idx].z_prev = bbox[idx].z;
		}

		double human2robot_distance = calculateDistance(bbox[idx].x,bbox[idx].y,bbox[idx].z,robot.x,robot.y,robot.z,0);
		cout<<"Distance: "<<human2robot_distance<<endl;
		if(human2robot_distance <= robot.r){
			regulator_array.push_back(0);
			bbox[idx].is_inside_roi = true;
		}
		else{
			bbox[idx].is_inside_roi = false;
		}

		// Velocity calculation and condition consideration //
		// * Humans can't move on y-axis unless they jump,
		// * so velocity takes distance with only x- and z-axis components
		double time_d = bbox[idx].time_current.toSec() - bbox[idx].time_prev.toSec();
		if(time_d > 0.3) {
			// Human (operator) velocity
			double human2human_prev_distance = calculateDistance(bbox[idx].x,bbox[idx].y,bbox[idx].z,bbox[idx].x_prev,bbox[idx].y_prev,bbox[idx].z_prev,0);
			double velocity = calculateVelocity(human2human_prev_distance, time_d);

			// Condition (1) //
			if (velocity < 0.1)
				robot.r = envelope_default_radius;
			else if (velocity >= 0.1 && velocity < 1)
				robot.r = envelope_default_radius * 1.1;
			else
				robot.r = envelope_default_radius * 1.5;

			double human2robot_distance_prev = calculateDistance(bbox[idx].x_prev,bbox[idx].y_prev,bbox[idx].z_prev,robot.x,robot.y,robot.z,0);
			double human2envelope_distance = human2robot_distance - robot.r;
			double radial_distance = human2robot_distance_prev - human2robot_distance;
			double radial_velocity = calculateVelocity(radial_distance, time_d);
			double tangential_velocity = sqrt(pow(velocity,2)-pow(radial_velocity,2));


			float velocity_vector[] = {
				bbox[idx].x-bbox[idx].x_prev,
				bbox[idx].y-bbox[idx].y_prev,
				bbox[idx].z-bbox[idx].z_prev,
			};
			float radial_velocity_vector[] = {
				robot.x-bbox[idx].x,
				robot.y-bbox[idx].y,
				robot.z-bbox[idx].z,
			};

			// Condition (2) //
			int regulator_value = 100;
			if(human2envelope_distance < 0) {
				regulator_value = 0;
			}
			else {
				if(velocity < 0.1) {
					regulator_value = 50;
				}
				else if(velocity >= 0.1 && velocity < 2.0) {
					regulator_value = 25;
				}
				else if(velocity >= 2.0) {
					regulator_value = 0;
				}
				else {
					regulator_value = 100;
				}

				if(radial_velocity > 0) {
					// * Angle between velocity vector and radial velocity vector
					double phi = acos( (velocity_vector[0]*radial_velocity_vector[0] +
						 								  velocity_vector[1]*radial_velocity_vector[1] +
													 	  velocity_vector[2]*radial_velocity_vector[2]) /
														 (sqrt(pow(velocity_vector[0],2)+pow(velocity_vector[1],2)+pow(velocity_vector[2],2)) *
															sqrt(pow(radial_velocity_vector[0],2)+pow(radial_velocity_vector[1],2)+pow(radial_velocity_vector[2],2))));

					// * Angle between |human to robot| line and
					// * tangent from human point to robot's envelope
					double alpha = asin(robot.r/human2robot_distance);
					if(phi <= alpha) {
						regulator_value = 0;
					}
				}
			}

			regulator_array.push_back(regulator_value);

			bbox[idx].time_prev = bbox[idx].time_current;
			bbox[idx].x_prev = bbox[idx].x;
			bbox[idx].y_prev = bbox[idx].y;
			bbox[idx].z_prev = bbox[idx].z;

			//test_file<<msg->header.stamp<<","<<velocity<<endl; //Velocity to csv file
			//test_file<<msg->header.stamp<<","<<human2robot_distance<<endl; // Distance to csv file
		}
	}

	// Publish only the lowest regulator value
	if(!regulator_array.empty()) {
		sort(regulator_array.begin(), regulator_array.end());
		regulator.value = regulator_array.at(0);
		regulator.header.stamp = ros::Time::now();
		regulator_publisher.publish(regulator);
		//test_file<<msg->header.stamp<<","<<min_regulator_value<<endl; //Regulator value to csv file
	}
	//test_file<<msg->header.stamp<<","<<human_idx_array.size()<<endl; //Detections to csv file

	// * Show depth image containing human points
	cv::imshow("Human points image", human_points_img);
	cv::waitKey(30);
}

double BoundingBoxDepthClass::calculateDistance(float x1, float y1, float z1,
																					float x2, float y2, float z2, bool y_flag)
{
	double distance = 0.0;
	if(!y_flag)
		 distance = sqrt(pow(x2-x1,2) + pow(z2-z1, 2));
	else
		 distance = sqrt(pow(x2-x1,2) + pow(y2-y1,2) + pow(z2-z1, 2));
	 return distance;
}


double BoundingBoxDepthClass::calculateVelocity(double distance, double time)
{
	return (distance / time);
}


float BoundingBoxDepthClass::findMedian(int x, int y)
{
	int n = 9;
	float array[n];
	float sum = 0;
	int step = 0;

	array[0] = depth_image.at<float>(y-1,x-1);
	array[1] = depth_image.at<float>(y-1,x);
	array[2] = depth_image.at<float>(y-1,x+1);
	array[3] = depth_image.at<float>(y,x-1);
	array[4] = depth_image.at<float>(y,x);
	array[5] = depth_image.at<float>(y,x+1);
	array[6] = depth_image.at<float>(y+1,x-1);
	array[7] = depth_image.at<float>(y+1,x);
	array[8] = depth_image.at<float>(y+1,x+1);

	sort(array, array + n);

	float median = array[(n-1) / 2];

	while(isnan(median) && step < n)
	{
		median = array[step];
		step++;
	}
	return median;
}


float BoundingBoxDepthClass::findAverage(int x, int y, bool flag)
{
	int n = 9;
	float array[n];
	float sum = 0;

	if(flag){
		array[0] = depth_image.at<float>(y-1,x-1);
		array[1] = depth_image.at<float>(y-1,x);
		array[2] = depth_image.at<float>(y-1,x+1);
		array[3] = depth_image.at<float>(y,x-1);
		array[4] = depth_image.at<float>(y,x);
		array[5] = depth_image.at<float>(y,x+1);
		array[6] = depth_image.at<float>(y+1,x-1);
		array[7] = depth_image.at<float>(y+1,x);
		array[8] = depth_image.at<float>(y+1,x+1);
	}
	else{
		array[0] = initial_depths[x-1][y-1];
		array[1] = initial_depths[x][y-1];
		array[2] = initial_depths[x+1][y-1];
		array[3] = initial_depths[x-1][y];
		array[4] = initial_depths[x][y];
		array[5] = initial_depths[x+1][y];
		array[6] = initial_depths[x-1][y+1];
		array[7] = initial_depths[x][y+1];
		array[8] = initial_depths[x+1][y+1];
	}

	for (int i = 0; i < n; i++)
		sum += array[i];

	return sum / n;
}
