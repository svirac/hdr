/*
	Node used for saving depth values in csv format of a static scene.
	Initialization must be run every time the camera moves to a different position
	or objects on the scene are rearranged.
	In order to deal with the Kinect's data loss, algorithm takes first 51 frames of the scene,
	which gives 51 depth values for each point on the scene. Saved depth value for one
	point is median calculated from 51 values. This step should reduce NaN depth values.
*/

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>

/* ROS */
#include <ros/ros.h>
#include "sensor_msgs/Image.h"
#include <image_transport/image_transport.h>
#include <std_msgs/String.h>
#include <boost/thread.hpp>
#include <ros/package.h>

/* OpenCV */
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>

using namespace std;

// Number of frames for initialization
const int maxFrames = 51;

cv::Mat depthImage;
cv::Mat depthImageArray[maxFrames];
cv::Mat medianDepthImage;

int frameNum = 0;

const int width = 639;
const int height = 479;

void depthCallback(const sensor_msgs::ImageConstPtr& msg)
{
	depthImage = cv_bridge::toCvShare(msg, "32FC1")->image;
	depthImageArray[frameNum] = depthImage.clone();
	frameNum++;

	if(frameNum == maxFrames)
		ros::shutdown();
}

float findMedian(float array[])
{
	sort(array, array + maxFrames);
	int step = (maxFrames-1)  /  2;
	float median = array[step];

	// If current median value is 0.0 or NaN, check next value, and so on
	while(median == 0.0 || isnan(median))
	{
		if(step == maxFrames)
			break;
		step++;
		median = array[step];
	}

	return median;
}


int main(int argc, char **argv)
{
  ros::init(argc,argv,"init_depth_scene");
  ros::NodeHandle nh("~");

	string depth_img_topic;
	string init_filename;

	nh.param("subscribers/depth_img_topic", depth_img_topic, std::string("/camera/depth_registered/image"));
	nh.param("init_depths_file", init_filename, std::string("kinect_depth_init.csv"));

	printf("\nWaiting for sensor messages...\n");
	cout<<depth_img_topic<<endl;
	sensor_msgs::ImageConstPtr msg = ros::topic::waitForMessage<sensor_msgs::Image>(depth_img_topic);
	printf("Ready! ");


	image_transport::ImageTransport it(nh);
	image_transport::Subscriber depth_sub = it.subscribe(depth_img_topic, 10, depthCallback);

	// Spins until 51 frames are received and saved in cv::Mat array
	while (ros::ok())
	{
		ros::spinOnce();
	}

	medianDepthImage = depthImage.clone();
	float array[maxFrames];
	float value = 0.0;

	for(int y = 0; y < height; y++){
		for(int x = 0; x < width; x++){
			for (int j = 0; j < maxFrames; j++) {
				array[j] = depthImageArray[j].at<float>(y,x);
			}
			value = findMedian(array);
			medianDepthImage.at<float>(y,x) = value;
	}}

	string pkg_path = ros::package::getPath("bbox_depth");
	//string initial_depths_path = pkg_path.append("/init/"+init_filename);
	string initial_depths_path = pkg_path.append(init_filename);

	ofstream file;
  file.open (initial_depths_path);
	file << medianDepthImage;
  file.close();

	cout << "Initialization finished!\n"<<endl;

  return 0;
}
