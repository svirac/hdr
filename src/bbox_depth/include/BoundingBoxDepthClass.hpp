#ifndef BOUNDINGBOXDEPTHCLASS_HPP
#define BOUNDINGBOXDEPTHCLASS_HPP

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <math.h>
#include <time.h>
#include <chrono>

/* OpenCV */
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>

/* ROS */
#include <ros/ros.h>
#include "sensor_msgs/Image.h"
#include <image_transport/image_transport.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Int64.h>
#include <boost/thread.hpp>
#include <ros/package.h>

/* Yolo */
#include "darknet_ros_msgs/BoundingBoxes.h"
#include "darknet_ros_msgs/BoundingBox.h"
#include "darknet_ros_msgs/ObjectCount.h"

/* Deep SORT */
#include "sort_track/IntList.h"
#include "sort_track/TrackBoundingBox.h"
#include "sort_track/TrackBoundingBoxes.h"
#include "sort_track/TrackCount.h"

/* BBox depth */
#include "bbox_depth/Regulator.h"


using namespace std;


const int bbox_count_MAX = 50;

class BoundingBoxDepthClass
{
	private:
		ros::NodeHandle nh;

		// Subiscribers //
		ros::Subscriber sub_yolo_count;
		ros::Subscriber sub_tracker_bbox;
		image_transport::Subscriber sub_rgb_image;
		image_transport::Subscriber sub_depth_image;

		// Publishers //
		ros::Publisher regulator_publisher;
		bbox_depth::Regulator regulator;

		string pkg_path;

		cv::Mat rgb_image;
		cv::Mat depth_image;

		int bbox_count;
		vector<int> human_idx_array;
		vector<int> human_idx_array_old;

		struct BBox{
			int id, xmin, xmax, ymin, ymax, center_x, center_y;
			float depth,depth_prev;
			float x, y, z;
			float x_prev, y_prev, z_prev;
			ros::Time time_current, time_prev;
			bool is_inside_roi;
		} bbox[bbox_count_MAX];

		// Topic names
		string regulator_topic;
		string rgb_img_topic;
		string depth_img_topic;
		string yolo_count_topic;
		string tracker_bbox_topic;

		float initial_depths[640][480];
		string initial_depths_filename;

		const int width;
		const int height;

		//Intrinsic parameters
		const float fx;
		const float fy;
		const float cx;
		const float cy;

		// Robot position
		struct Robot{
			float x, y, z;
			float r;
		} robot;

		const float envelope_default_radius;

		int frame_counter;

		// Variables for removing tracking detection
		int last_frame;
		int current_frame;
		int frame_diff;
		int frame_threshold;

		string test_filename;
		ofstream test_file;

	public:
		BoundingBoxDepthClass(const ros::NodeHandle &node_handle);
		~BoundingBoxDepthClass() = default;

		void init();

		/* Subscriber callbacks */
		void yoloCountCallback(const darknet_ros_msgs::ObjectCount::ConstPtr& msg);
		void trackerBboxCallback(const sort_track::TrackBoundingBoxes::ConstPtr& msg);
		void rgbImageCallback(const sensor_msgs::ImageConstPtr& msg);
		void depthImageCallback(const sensor_msgs::ImageConstPtr& msg);


		/* Methods */
		void loadInitialDepths(const int width, const int height);
		float findMedian(int x, int y);
		float findAverage(int x, int y, bool flag);

		double calculateDistance(float x1, float y1, float z1, float x2, float y2, float z2, bool y_flag);
		double calculateVelocity(double distance, double time);
};

#endif
