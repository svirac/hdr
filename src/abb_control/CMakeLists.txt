cmake_minimum_required(VERSION 3.0.2)
project(abb_control)

add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  moveit_ros_planning_interface
  moveit_visual_tools
  rviz_visual_tools
  bbox_depth
)

install(DIRECTORY
  launch
  DESTINATION share/${PROJECT_NAME}/
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

catkin_package(
	CATKIN_DEPENDS roscpp std_msgs bbox_depth
)

add_executable(infinite_movement src/infinite_movement.cpp)
add_dependencies(infinite_movement ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})
target_link_libraries(infinite_movement ${catkin_LIBRARIES})

