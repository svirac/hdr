
#include <stdio.h>
#include <iostream>
#include <string>


#include <ros/ros.h>
#include "bbox_depth/Regulator.h"

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <rviz_visual_tools/rviz_visual_tools.h>
#include <moveit_visual_tools/moveit_visual_tools.h>


using namespace std;

int regulator1_value = 100;
int regulator2_value = 100;
ros::Time regulator1_time;
ros::Time regulator2_time;

/*
void regulator1_callback(const bbox_depth::Regulator::ConstPtr& msg)
{
	::regulator1_value = msg->value;
	::regulator1_time = msg->header.stamp;
}

void regulator2_callback(const bbox_depth::Regulator::ConstPtr& msg)
{
	::regulator2_value = msg->value;
	::regulator2_time = msg->header.stamp;
}*/

void regulator_callback(const bbox_depth::Regulator::ConstPtr& msg)
{
	::regulator1_value = msg->value;
	::regulator1_time = msg->header.stamp;
}


int main(int argc, char** argv)
{

	ros::init(argc, argv, "move_abb_node");
	ros::NodeHandle node_handle;
	ros::AsyncSpinner spinner(1);
	spinner.start();

	ros::Subscriber sub_reg = node_handle.subscribe("/bbox_depth/regulator_value", 1, regulator_callback); //when using 1 camera
	//ros::Subscriber sub_regulator1 = node_handle.subscribe("/kinect1/bbox_depth/regulator_value", 1, regulator1_callback);
	//ros::Subscriber sub_regulator2 = node_handle.subscribe("/kinect2/bbox_depth/regulator_value", 1, regulator2_callback);

	static const std::string PLANNING_GROUP = "manipulator";
	moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
	moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
	const robot_state::JointModelGroup* joint_model_group =
    move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);
	moveit::planning_interface::MoveGroupInterface::Plan my_plan;
	moveit::core::RobotStatePtr current_state = move_group.getCurrentState();
	std::vector<double> joint_group_positions;
	current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

	int regulator_value = 100;
	int step = 0;
	while(ros::ok())
	{
		ros::spinOnce();

		/* // Only for 2 cameras
		if(::regulator1_value <= ::regulator2_value)
			regulator_value = ::regulator1_value;
		else
			regulator_value = ::regulator2_value;
		*/
		if(ros::Time::now().toSec() - ::regulator1_time.toSec() > 2.0){
			regulator_value = 100;
		}
		else{
			regulator_value = ::regulator1_value;
		}

		if(regulator_value == 0)
			continue;
			//move_group.setMaxAccelerationScalingFactor(0.0);
		else if(regulator_value == 25)
			move_group.setMaxAccelerationScalingFactor(0.05);
		else if(regulator_value == 50)
			move_group.setMaxAccelerationScalingFactor(0.2);
		else
			move_group.setMaxAccelerationScalingFactor(0.4);


		if(step==0)
		{
			joint_group_positions[0] = -0.8;  // radians
			joint_group_positions[1] = -0.2;
		}
		else if(step==1)
		{
			joint_group_positions[0] = -0.4;
			joint_group_positions[1] = -0.1;
		}
		else if(step==2)
		{
			joint_group_positions[0] = 0.0;
			joint_group_positions[1] = 0.0;
		}
		else if(step==3)
		{
			joint_group_positions[0] = 0.4;
			joint_group_positions[1] = 0.1;
		}
		else if(step==4)
		{
			joint_group_positions[0] = 0.8;
			joint_group_positions[1] = 0.2;
		}
		else if(step==5)
		{
			joint_group_positions[0] = 0.4;
			joint_group_positions[1] = 0.1;
		}
		else if(step==6)
		{
			joint_group_positions[0] = 0.0;
			joint_group_positions[1] = 0.0;
		}
		else if(step==7)
		{
			joint_group_positions[0] = -0.4;
			joint_group_positions[1] = 0.1;
		}
		move_group.setJointValueTarget(joint_group_positions);
		move_group.plan(my_plan);
		move_group.move();
		//step = !step;
		step++;
		if(step >=8) step = 0;
	}

	ros::shutdown();

	return 0;
}
