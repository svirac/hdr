#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32

def callback(data):
    print(data.data)
    
rospy.init_node('listener')
rospy.Subscriber("/distance", Float32, callback)
rospy.spin()