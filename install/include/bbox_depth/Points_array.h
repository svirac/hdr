// Generated by gencpp from file bbox_depth/Points_array.msg
// DO NOT EDIT!


#ifndef BBOX_DEPTH_MESSAGE_POINTS_ARRAY_H
#define BBOX_DEPTH_MESSAGE_POINTS_ARRAY_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <bbox_depth/Points_coords.h>

namespace bbox_depth
{
template <class ContainerAllocator>
struct Points_array_
{
  typedef Points_array_<ContainerAllocator> Type;

  Points_array_()
    : points_coords()  {
    }
  Points_array_(const ContainerAllocator& _alloc)
    : points_coords(_alloc)  {
  (void)_alloc;
    }



   typedef std::vector< ::bbox_depth::Points_coords_<ContainerAllocator> , typename ContainerAllocator::template rebind< ::bbox_depth::Points_coords_<ContainerAllocator> >::other >  _points_coords_type;
  _points_coords_type points_coords;





  typedef boost::shared_ptr< ::bbox_depth::Points_array_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::bbox_depth::Points_array_<ContainerAllocator> const> ConstPtr;

}; // struct Points_array_

typedef ::bbox_depth::Points_array_<std::allocator<void> > Points_array;

typedef boost::shared_ptr< ::bbox_depth::Points_array > Points_arrayPtr;
typedef boost::shared_ptr< ::bbox_depth::Points_array const> Points_arrayConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::bbox_depth::Points_array_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::bbox_depth::Points_array_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace bbox_depth

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'bbox_depth': ['/home/damian/catkin_ws/src/bbox_depth/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::bbox_depth::Points_array_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::bbox_depth::Points_array_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::bbox_depth::Points_array_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::bbox_depth::Points_array_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::bbox_depth::Points_array_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::bbox_depth::Points_array_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::bbox_depth::Points_array_<ContainerAllocator> >
{
  static const char* value()
  {
    return "d740a7bcd2867f8164258ffb96a46238";
  }

  static const char* value(const ::bbox_depth::Points_array_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xd740a7bcd2867f81ULL;
  static const uint64_t static_value2 = 0x64258ffb96a46238ULL;
};

template<class ContainerAllocator>
struct DataType< ::bbox_depth::Points_array_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bbox_depth/Points_array";
  }

  static const char* value(const ::bbox_depth::Points_array_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::bbox_depth::Points_array_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Points_coords[] points_coords\n\
\n\
================================================================================\n\
MSG: bbox_depth/Points_coords\n\
int32 x\n\
int32 y\n\
";
  }

  static const char* value(const ::bbox_depth::Points_array_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::bbox_depth::Points_array_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.points_coords);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Points_array_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::bbox_depth::Points_array_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::bbox_depth::Points_array_<ContainerAllocator>& v)
  {
    s << indent << "points_coords[]" << std::endl;
    for (size_t i = 0; i < v.points_coords.size(); ++i)
    {
      s << indent << "  points_coords[" << i << "]: ";
      s << std::endl;
      s << indent;
      Printer< ::bbox_depth::Points_coords_<ContainerAllocator> >::stream(s, indent + "    ", v.points_coords[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // BBOX_DEPTH_MESSAGE_POINTS_ARRAY_H
