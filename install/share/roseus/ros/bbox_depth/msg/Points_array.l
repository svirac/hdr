;; Auto-generated. Do not edit!


(when (boundp 'bbox_depth::Points_array)
  (if (not (find-package "BBOX_DEPTH"))
    (make-package "BBOX_DEPTH"))
  (shadow 'Points_array (find-package "BBOX_DEPTH")))
(unless (find-package "BBOX_DEPTH::POINTS_ARRAY")
  (make-package "BBOX_DEPTH::POINTS_ARRAY"))

(in-package "ROS")
;;//! \htmlinclude Points_array.msg.html


(defclass bbox_depth::Points_array
  :super ros::object
  :slots (_points_coords ))

(defmethod bbox_depth::Points_array
  (:init
   (&key
    ((:points_coords __points_coords) (let (r) (dotimes (i 0) (push (instance bbox_depth::Points_coords :init) r)) r))
    )
   (send-super :init)
   (setq _points_coords __points_coords)
   self)
  (:points_coords
   (&rest __points_coords)
   (if (keywordp (car __points_coords))
       (send* _points_coords __points_coords)
     (progn
       (if __points_coords (setq _points_coords (car __points_coords)))
       _points_coords)))
  (:serialization-length
   ()
   (+
    ;; bbox_depth/Points_coords[] _points_coords
    (apply #'+ (send-all _points_coords :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bbox_depth/Points_coords[] _points_coords
     (write-long (length _points_coords) s)
     (dolist (elem _points_coords)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bbox_depth/Points_coords[] _points_coords
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _points_coords (let (r) (dotimes (i n) (push (instance bbox_depth::Points_coords :init) r)) r))
     (dolist (elem- _points_coords)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get bbox_depth::Points_array :md5sum-) "d740a7bcd2867f8164258ffb96a46238")
(setf (get bbox_depth::Points_array :datatype-) "bbox_depth/Points_array")
(setf (get bbox_depth::Points_array :definition-)
      "Points_coords[] points_coords

================================================================================
MSG: bbox_depth/Points_coords
int32 x
int32 y

")



(provide :bbox_depth/Points_array "d740a7bcd2867f8164258ffb96a46238")


