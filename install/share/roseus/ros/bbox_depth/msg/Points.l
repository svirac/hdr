;; Auto-generated. Do not edit!


(when (boundp 'bbox_depth::Points)
  (if (not (find-package "BBOX_DEPTH"))
    (make-package "BBOX_DEPTH"))
  (shadow 'Points (find-package "BBOX_DEPTH")))
(unless (find-package "BBOX_DEPTH::POINTS")
  (make-package "BBOX_DEPTH::POINTS"))

(in-package "ROS")
;;//! \htmlinclude Points.msg.html


(defclass bbox_depth::Points
  :super ros::object
  :slots (_coords ))

(defmethod bbox_depth::Points
  (:init
   (&key
    ((:coords __coords) (let (r) (dotimes (i 0) (push (instance bbox_depth::Coords :init) r)) r))
    )
   (send-super :init)
   (setq _coords __coords)
   self)
  (:coords
   (&rest __coords)
   (if (keywordp (car __coords))
       (send* _coords __coords)
     (progn
       (if __coords (setq _coords (car __coords)))
       _coords)))
  (:serialization-length
   ()
   (+
    ;; bbox_depth/Coords[] _coords
    (apply #'+ (send-all _coords :serialization-length)) 4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bbox_depth/Coords[] _coords
     (write-long (length _coords) s)
     (dolist (elem _coords)
       (send elem :serialize s)
       )
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bbox_depth/Coords[] _coords
   (let (n)
     (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4)
     (setq _coords (let (r) (dotimes (i n) (push (instance bbox_depth::Coords :init) r)) r))
     (dolist (elem- _coords)
     (send elem- :deserialize buf ptr-) (incf ptr- (send elem- :serialization-length))
     ))
   ;;
   self)
  )

(setf (get bbox_depth::Points :md5sum-) "9a00924c5b980fe809fce34d3b5d9735")
(setf (get bbox_depth::Points :datatype-) "bbox_depth/Points")
(setf (get bbox_depth::Points :definition-)
      "Coords[] coords

================================================================================
MSG: bbox_depth/Coords
int32 x
int32 y

")



(provide :bbox_depth/Points "9a00924c5b980fe809fce34d3b5d9735")


