// Auto-generated. Do not edit!

// (in-package bbox_depth.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;
let Points_coords = require('./Points_coords.js');

//-----------------------------------------------------------

class Points_array {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.points_coords = null;
    }
    else {
      if (initObj.hasOwnProperty('points_coords')) {
        this.points_coords = initObj.points_coords
      }
      else {
        this.points_coords = [];
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Points_array
    // Serialize message field [points_coords]
    // Serialize the length for message field [points_coords]
    bufferOffset = _serializer.uint32(obj.points_coords.length, buffer, bufferOffset);
    obj.points_coords.forEach((val) => {
      bufferOffset = Points_coords.serialize(val, buffer, bufferOffset);
    });
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Points_array
    let len;
    let data = new Points_array(null);
    // Deserialize message field [points_coords]
    // Deserialize array length for message field [points_coords]
    len = _deserializer.uint32(buffer, bufferOffset);
    data.points_coords = new Array(len);
    for (let i = 0; i < len; ++i) {
      data.points_coords[i] = Points_coords.deserialize(buffer, bufferOffset)
    }
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += 8 * object.points_coords.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a message object
    return 'bbox_depth/Points_array';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return 'd740a7bcd2867f8164258ffb96a46238';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    Points_coords[] points_coords
    
    ================================================================================
    MSG: bbox_depth/Points_coords
    int32 x
    int32 y
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Points_array(null);
    if (msg.points_coords !== undefined) {
      resolved.points_coords = new Array(msg.points_coords.length);
      for (let i = 0; i < resolved.points_coords.length; ++i) {
        resolved.points_coords[i] = Points_coords.Resolve(msg.points_coords[i]);
      }
    }
    else {
      resolved.points_coords = []
    }

    return resolved;
    }
};

module.exports = Points_array;
