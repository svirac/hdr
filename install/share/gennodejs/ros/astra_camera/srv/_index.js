
"use strict";

let GetUVCWhiteBalance = require('./GetUVCWhiteBalance.js')
let SetIRFlood = require('./SetIRFlood.js')
let GetDeviceType = require('./GetDeviceType.js')
let SetIRExposure = require('./SetIRExposure.js')
let GetUVCGain = require('./GetUVCGain.js')
let GetIRGain = require('./GetIRGain.js')
let SwitchIRCamera = require('./SwitchIRCamera.js')
let SetLDP = require('./SetLDP.js')
let SetUVCGain = require('./SetUVCGain.js')
let SetLaser = require('./SetLaser.js')
let GetUVCExposure = require('./GetUVCExposure.js')
let SetUVCExposure = require('./SetUVCExposure.js')
let SetUVCWhiteBalance = require('./SetUVCWhiteBalance.js')
let GetCameraInfo = require('./GetCameraInfo.js')
let ResetIRExposure = require('./ResetIRExposure.js')
let GetIRExposure = require('./GetIRExposure.js')
let GetSerial = require('./GetSerial.js')
let ResetIRGain = require('./ResetIRGain.js')
let SetIRGain = require('./SetIRGain.js')

module.exports = {
  GetUVCWhiteBalance: GetUVCWhiteBalance,
  SetIRFlood: SetIRFlood,
  GetDeviceType: GetDeviceType,
  SetIRExposure: SetIRExposure,
  GetUVCGain: GetUVCGain,
  GetIRGain: GetIRGain,
  SwitchIRCamera: SwitchIRCamera,
  SetLDP: SetLDP,
  SetUVCGain: SetUVCGain,
  SetLaser: SetLaser,
  GetUVCExposure: GetUVCExposure,
  SetUVCExposure: SetUVCExposure,
  SetUVCWhiteBalance: SetUVCWhiteBalance,
  GetCameraInfo: GetCameraInfo,
  ResetIRExposure: ResetIRExposure,
  GetIRExposure: GetIRExposure,
  GetSerial: GetSerial,
  ResetIRGain: ResetIRGain,
  SetIRGain: SetIRGain,
};
